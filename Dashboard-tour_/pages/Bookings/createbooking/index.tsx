import Head from "next/head"

import { Box,styled ,Container,Grid, Card, CardHeader,Divider, CardContent, TextField, Button} from '@mui/material';
import Item from "@/content/principal/components/ItemG";
import SidebarLayout from "@/layouts/SidebarLayout";
import React, {  useState } from "react";
import { Createtravel } from "@/api/service";

const RootWrapper = styled(Box)(
    ({ theme }) => `
         height: calc(100vh - ${theme.header.height});
         display: flex;
  `
  );
interface FormData{
  title: string;
  descripcion: string;
  body: string;
}
  
 function createbooking(){
   const [ formData, setFormData ] = useState<FormData>({ title: '',descripcion:'',body:''});

   function handleInputChange(event: React.ChangeEvent<HTMLInputElement>){
    ///{title,"lo que escribo"} = event.target
      const { name ,value }= event.target;
      setFormData({ ...formData,[ name]:value})
   }
   const handleSubmit= (event) => {
    
    console.log('success',formData );
    
    Createtravel(formData).then((result)=>{
      alert("creado correctamente")
    }).catch(e=>{
      alert(" error al crear")
    })
    event.preventDefault();
  }
  
    return(
        <>
      <Head>
        <title>CREATE BOOKING</title>
      </Head>
      <div className="dashboard-principal">
      <RootWrapper className="Mui-FixedWrapper">
        <Container maxWidth="lg" style={{marginTop:40}}>
            <Grid container direction="row" justifyContent="center" alignItems="stretch" spacing={3}>
            <Grid item xs={12}>
            <Card>
              <CardHeader title="Crear Reserva" />
              <Divider />
              <CardContent>
                <form onSubmit={handleSubmit}>
                <Grid className="container" container spacing={2} sx={{ flexGrow: 1 }}>
                                
                                <Grid  xs={6}>
                                    <Item style={{paddingTop:"2em",marginRight:"2em"}}>
                                    <TextField value={formData.title} name="title" onChange={handleInputChange} style={{width:"100%"}}  /* required id="outlined-required" */ label="Titulo" placeholder="Titulo de publicacion" />
                                    </Item>
                                </Grid>
                                <Grid xs={6}>
                                    <Item style={{paddingTop:"2em",width:"100%",margin:"auto"}}> <TextField value={formData.descripcion} name="descripcion" onChange={handleInputChange}  /* required id="outlined-required" */ multiline maxRows={4} label="Descripcion" placeholder="Descripcion" /></Item>
                                </Grid>
                                
                                <Grid   xs={12}>
                                    <Item style={{paddingTop:"2em"}}><TextField value={formData.body} name="body" onChange={handleInputChange} style={{width:"100%"}}  /* required id="outlined-required" */ multiline maxRows={10} label="Body" placeholder="Descripcion del body" /></Item>
                                </Grid>
                                <Grid>
                               ---aqui vas       
                                </Grid>
                                
                                </Grid>
                                

                                
                                <div style={{display:"flex",justifyContent:"center",alignItems:"center",paddingTop:"2em"}}>
                                <Button type="submit" className="button-header" variant="contained">
                                    <span>Guardar Reserva</span>
                                </Button>
                                </div>
                </form>
                            
              </CardContent>
            </Card>
          </Grid>
            </Grid>
        </Container>
       
      </RootWrapper>
      </div>
    </>
    )
}

createbooking.getLayout = (page) => (
    <SidebarLayout>{page}</SidebarLayout>
  );
  
  export default createbooking;