/* 

import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useRouter } from 'next/router';


function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();



export default function SignInSide() {
   
  const router = useRouter()
  function click(e) {
    e.preventDefault();
    router.push("/dashboards/crypto");
  }
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({
      email: data.get('email'),
      password: data.get('password'),
    });
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: '100vh' }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: 'url(/img/ss.webp)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: (t) =>
              t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1,width:"5em",height:"5em", bgcolor: 'secondary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button type="submit"  onClick={click} fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}> Iniciar Sesion </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
              <Copyright sx={{ mt: 5 }} />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
  
} */

import * as React from 'react';

import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';

import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useRouter } from 'next/router';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LockIcon from '@mui/icons-material/Lock';
import { Login } from '../../src/api/service.js';
function Copyright(props: any) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

export default function SignIn() {

  const router = useRouter()
   async function click(e) {
    e.preventDefault();
    router.push("/dashboards/crypto");
   /*  await Login().then(result=>{
      alert(JSON.stringify(result))
    }).catch(e=>{
      alert(JSON.stringify(e))
    })  */
  }
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log({
      email: data.get('email'),
      password: data.get('password'),
    });
  };

  return (
    <div className="login">
      
      <ThemeProvider   theme={defaultTheme}>
     <Container style={{marginTop:"2.5em",borderRadius:"2.5em" ,
           backdropFilter:"blur(3px)",
           backgroundPosition: 'center',height:"80vh"}} className='signin' component="main" maxWidth="xs">
       <CssBaseline />
       <div >
       <Box
         sx={{
           marginTop: 8,
           display: 'flex',
           flexDirection: 'column',
           alignItems: 'center',
         }}
       >

         {/* <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
           <LockOutlinedIcon />
         </Avatar> */}
         <Typography style={{paddingTop:"1em",color:"white",fontSize:"2em",fontWeight:"bold"}} component="h1" variant="h5">
          BIENVENIDO
         </Typography>
         <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
           {/* <TextField
             margin="normal"
             
             required
             fullWidth
             id="email"
             label="Email Address"
             name="email"
             autoComplete="email"
             autoFocus
           /> */}
            <div style={{display:"flex",alignItems:"center",paddingTop:"2em",paddingBottom:"1.5em" ,paddingLeft:"0",marginLeft:"0"}}>
           <AccountCircleIcon sx={{ fontSize: 40, color: "white" }} />
           <input 
            
             required
             
             name="password"
             placeholder='EMAIL'
             type="text"
             id="password"
             autoComplete="current-password"
             style={{width:"25em"}}
           />
           </div>
          
           <div style={{display:"flex",alignItems:"center"}}>
           <LockIcon sx={{ fontSize: 40, color: "white" }} />
           
           <input            
             required
             name="password"
             placeholder='CONTRASEÑA'
             type="password"
             id="password"
             autoComplete="current-password"
             
           />
           </div>
            <div className="button-login">
            <Button  type="submit"  fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}  style={{marginTop:"1.5em"}} onClick={click}>INICIAR SESION</Button>
            </div>
             <FormControlLabel
             control={<Checkbox value="remember" color="secondary" />}
             label="Remember me"
           />
           <Grid container>
             <Grid item xs>
               <div style={{display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column"}}>
               <Link href="#" variant="body2">
                 ¿OLVIDASTE LA CONTRASEÑA? 
               </Link>
               <div style={{paddingTop:"2em"}}>
               <Link href="#" variant="body2">
                 {"REGISTRATE"}
               </Link>
               </div>
               </div>
             </Grid>
             
           </Grid>
         </Box>
       </Box>
       </div>
       <Copyright sx={{ mt: 8, mb: 4 }} />
     </Container>
   </ThemeProvider>
    
    </div>
  );
}