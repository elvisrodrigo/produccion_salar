import Head from 'next/head';

import SidebarLayout from '@/layouts/SidebarLayout';

import PageHeader from '@/content/dashboards/crypto/PageHeader';
import PageTitleWrapper from '@/components/PageTitleWrapper';
import { Container, Grid } from '@mui/material';
import Footer from '@/components/Footer';
import Item from '@/content/principal/components/ItemG';
import AccountBalance from '@/content/dashboards/crypto/AccountBalance';
import Wallets from '@/content/dashboards/crypto/Wallets';
import AccountSecurity from '@/content/dashboards/crypto/AccountSecurity';
import WatchList from '@/content/dashboards/crypto/WatchList';

function DashboardCrypto() {
  return (
    <>
      <Head>
        <title>Welcome</title>
      </Head>
      <div className='dashboard-principal'>
      <PageTitleWrapper >
        {/* <PageHeader /> */}
            <h1 style={{textTransform:"uppercase"}}>EL SALAR DE UYUNI EN BOLIVIA </h1>
            <h1 style={{paddingBottom:"3em"}}> POTOSI</h1>
            <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={6}>
              <Item>
                <p style={{fontSize:"1.2em"}}>El Salar de Uyuni, es el mayor lago de sal del mundo, es un lugar visualmente cautivante y uno de los 
                  más increíblemente y hermosos lugares que jamás haya visto en Bolivia o América del Sur. Es aproximadamente 
                  25 veces el tamaño de las salinas de Bonneville en los Estados Unidos y se estima que contiene 10 mil millones
                   de toneladas de sal, con menos de 25000 toneladas extraídas anualmente.</p>
                
              </Item>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Item></Item>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Item>< img width={"100%"} src="/img/dash1.jpg" alt="" /></Item>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Item></Item>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Item><p style={{fontSize:"1.2em"}}>A una altitud de 3 656 metros (11995 pies) sobre el nivel del mar, el Salar de Uyuni abarca unos asombrosos 
                10582 kilómetros cuadrados (4086 millas). Se encuentra en las regiones de Potosí y de Oruro en el sudoeste de Bolivia, 
                cerca de la cresta de los Andes. El salar de Uyuni se formó como resultado de las transformaciones entre varios lagos
                 pre-históricos. Hace unos 40 000 años, el área era parte del Lago Minchin, un gigantesco lago prehistórico. Cuando
                  el lago se secó, dejó atrás dos lagos el Poopó y el moderno Uru Uru. Además, dos grandes "desiertos" de sal fueron
                   creados como resultado de este proceso: el Salar de Coipasa y el mucho más grande Salar de Uyuni.
            </p></Item>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <Item></Item>
            </Grid>
          </Grid>
      </PageTitleWrapper>
      </div>
      {/* <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={4}
        >
          <Grid item xs={12}>
            <AccountBalance />
          </Grid>
          <Grid item lg={8} xs={12}>
            <Wallets />
          </Grid>
          <Grid item lg={4} xs={12}>
            <AccountSecurity />
          </Grid>
          <Grid item xs={12}>
            <WatchList />
          </Grid>
        </Grid>
      </Container> */}
      <Footer />
    </>
  );
}

DashboardCrypto.getLayout = (page) => <SidebarLayout>{page}</SidebarLayout>;

export default DashboardCrypto;
