import {
  Typography,
  Box,
  Card,
  Container,
  Button,
  styled
} from '@mui/material';
import type { ReactElement } from 'react';
import BaseLayout from 'src/layouts/BaseLayout';
import Principal from '@/content/principal/Principal';
import Hero from '@/content/overview/Hero';
import Link from 'src/components/Link';
import Head from 'next/head';

import Logo from 'src/components/LogoSign';


const HeaderWrapper = styled(Card)(
  ({ theme }) => `
  width: 100%;
  display: flex;
  align-items: center;
  height: ${theme.spacing(10)};
  margin-bottom: ${theme.spacing(10)};
`
);

const OverviewWrapper = styled(Box)(
  ({ theme }) => `
    
  overflow-x: hidden; 
`
);

function Overview() {
  return (
    <>
    {/* <Principal/> */}
    
    <OverviewWrapper>
     
     <Hero />
     
   </OverviewWrapper> 
    
    </>
    
  );
}

export default Overview;

Overview.getLayout = function getLayout(page: ReactElement) {
  return <BaseLayout>{page}</BaseLayout>;
};
