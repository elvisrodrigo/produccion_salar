import DrawerAppBar from "@/content/principal/components/navbar"
import Footer from "@/content/principal/components/Footer"
import { Grid } from "@mui/material"
import  Item from "@/content/principal/components/ItemG"
import FestivalIcon from '@mui/icons-material/Festival';
import TouchAppIcon from '@mui/icons-material/TouchApp';
import PhotoCameraBackIcon from '@mui/icons-material/PhotoCameraBack';
import LightModeIcon from '@mui/icons-material/LightMode';
export default function ExplorerRouter(){
    return(
        <>
        
        <DrawerAppBar/>
        
        <section style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} className="explorer-one" >
            <div >
            <h2 style={{textAlign:"center",color:"white"}}>EXPLORER</h2>
            <h1 style={{textAlign:"center",color:"white"}} className="about-one-section">VISIT US</h1>
            </div>
            <svg className="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#Fff" fillOpacity="1" d="M0,64L80,96C160,128,320,192,480,197.3C640,203,800,149,960,117.3C1120,85,1280,75,1360,69.3L1440,64L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"></path>
            </svg>
            
        </section>
        <div style={{backgroundColor:"white"}}>
        <h1 className="explorer-h" style={{fontSize:"5em" ,textAlign:"center"}}>What we do</h1>
        <p style={{fontSize:"1.3em" ,textAlign:"center"}}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum molestias eligendi 
        ipsum rem. Praesentium, doloribus.</p>
        <Grid  container spacing={1}>
            <Grid xs={12} sm={12} md={6}>
                <Item >
                    <img style={{width:"100%",padding:"5em"}} src="/img/list9.jpg" alt="" />
                </Item>
            </Grid>
            
            <Grid xs={12} sm={12} md={6}>
                <Item style={{width:"70%",margin:"auto"}} >
               <div style={{display:"flex",flexDirection:"column",alignItems:"center",justifyContent:"center",paddingTop:"2em"}}>
               <div style={{display:"flex"}}>
                <div style={{display:"flex" ,justifyContent:"center",alignItems:"center"}}>
                <FestivalIcon  sx={{ color:"LightGreen",fontSize: 60 }} />
                </div>
                <div style={{flexDirection:"column",paddingLeft:"2em"}}>
                    <h1 >hotel tourism</h1>
                    <p style={{}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magnam, ipsam! Lorem ipsum dolor sit amet.</p>
                </div>
                </div>
                <div style={{display:"flex"}}>
                <div style={{display:"flex" ,justifyContent:"center",alignItems:"center"}}>
                <TouchAppIcon  sx={{ color:"back",fontSize: 60 }} />
                </div>
                <div style={{flexDirection:"column",paddingLeft:"2em"}}>
                    <h1 >ubicacion</h1>
                    <p style={{}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magnam, ipsam! Lorem ipsum dolor sit amet.</p>
                </div>
                </div>
                <div style={{display:"flex"}}>
                <div style={{display:"flex" ,justifyContent:"center",alignItems:"center"}}>
                <PhotoCameraBackIcon  sx={{ color:"LightSeaGreen",fontSize: 60 }} />
                </div>
                <div style={{flexDirection:"column",paddingLeft:"2em"}}>
                    <h1 >camera</h1>
                    <p style={{}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magnam, ipsam! Lorem ipsum dolor sit amet.</p>
                </div>
                </div>
                <div style={{display:"flex"}}>
                <div style={{display:"flex" ,justifyContent:"center",alignItems:"center"}}>
                <LightModeIcon  sx={{ color:"orange",fontSize: 60 }} />
                </div>
                <div style={{flexDirection:"column",paddingLeft:"2em"}}>
                    <h1 >desert</h1>
                    <p style={{}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magnam, ipsam! Lorem ipsum dolor sit amet.</p>
                </div>
                </div>
               </div>
                
                </Item>
            </Grid>
            </Grid>
        </div>
        <section className="explorer-backgroud">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#fff" fillOpacity="1" d="M0,224L40,224C80,224,160,224,240,234.7C320,245,400,267,480,282.7C560,299,640,309,720
            ,288C800,267,880,213,960,165.3C1040,117,1120,75,1200,96C1280,117,1360,203,1400,245.3L1440,288L1440,0L1400,0C1360,0,1280,0,1200,
            0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path>
            </svg>
            <Grid  container spacing={1}>
            <Grid xs={12} sm={12} md={6}>
                <Item style={{width:"70%",margin:"auto"}} >
                   <h1 style={{color:"white",fontSize:"3em",paddingBottom:"1.5em"}}>THE PARADISE</h1>
                  
                   <p style={{display:"flex",alignItems:"center",justifyContent:"center",color:"white",fontSize:"1.2em"}}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro, 
                    eius voluptatem ipsa deserunt totam consequatur. Qui placeat, animi tempora
                     adipisci nam tenetur quisquam ad excepturi nemo possimus eligendi ratione non, aliquid
                      modi ex, blanditiis commodi soluta labore corrupti? Temporibus, debitis!</p>
                </Item>
            </Grid>
            
            <Grid xs={12} sm={12} md={6}>
                <Item  >
                <iframe  style={{marginLeft:"2em",width:"90%",height:"20em"}} src="https://www.youtube.com/embed/J-cI8EhFNMA" title="YouTube video player" 
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
                </Item>
            </Grid>
            </Grid>
            <svg style={{marginBottom:"-1em"}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#fff" fillOpacity="1" d="M0,64L48,74.7C96,85,192,107,288,144C384,181,480,235,576,218.7C672,203,768,117,864,117.3C960,117,
            1056,203,1152,224C1248,245,1344,203,1392,181.3L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,
            672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
            </svg>
            </section>
            <section>
            <h1 className="explorer-h" style={{fontSize:"5em" ,textAlign:"center"}}>Our Service</h1>
            <p style={{fontSize:"1.3em" ,textAlign:"center",paddingBottom:"2em"}}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum molestias eligendi 
            ipsum rem. Praesentium, doloribus.</p>
            <div className="container">
            <Grid style={{paddingTop:"4em"}} container spacing={1}>
            <Grid xs={12} sm={8} md={5}>
                <Item >
                   <div style={{width:"80%",margin:"auto"}}>
                   <img style={{width:"100%"}} src="/img/list1.jpg" alt="" />
                   </div>
                </Item>
            </Grid>
            
            <Grid xs={12} sm={12} md={7}>
                <Item  >
                <div style={{width:"85%",margin:"auto"}}>
                <FestivalIcon  sx={{ color:"LightGreen",fontSize: 60 }} />
                <h1 style={{fontWeight:"bold",fontSize:"2em",textTransform:"capitalize"}} >hotel tourism</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore laboriosam dignissimos minus saepe aliquam 
                    voluptates voluptatem, illum dicta atque temporibus, id necessitatibus modi distinctio, dolore a eligendi laudantium
                     expedita quam cumque sit? Exercitationem numquam adipisci totam recusandae eos! Culpa, perferendis?</p>
                </div>
                </Item>
            </Grid>
            </Grid>
            <Grid style={{paddingTop:"4em"}} container spacing={1}>
            <Grid xs={12} sm={8} md={5}>
                <Item  >
                   <div style={{width:"80%",margin:"auto"}}>
                   <img style={{width:"100%"}} src="/img/list2.jpg" alt="" />
                   </div>
                </Item>
            </Grid>
            
            <Grid xs={12} sm={12} md={7}>
                <Item  >
                <div style={{width:"85%",margin:"auto"}}>
                <TouchAppIcon  sx={{ color:"back",fontSize: 60 }} />
                <h1 style={{fontWeight:"bold",fontSize:"2em",textTransform:"capitalize"}} >ubicacion</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore laboriosam dignissimos minus saepe aliquam 
                    voluptates voluptatem, illum dicta atque temporibus, id necessitatibus modi distinctio, dolore a eligendi laudantium
                     expedita quam cumque sit? Exercitationem numquam adipisci totam recusandae eos! Culpa, perferendis?</p>
                </div>
                </Item>
            </Grid>
            </Grid>
            <Grid style={{paddingTop:"4em"}} container spacing={1}>
            <Grid xs={12} sm={8} md={5}>
                <Item  >
                   <div style={{width:"80%",margin:"auto"}}>
                   <img style={{width:"100%"}} src="/img/list3.jpg" alt="" />
                   </div>
                </Item>
            </Grid>
            
            <Grid xs={12} sm={12} md={7}>
                <Item  >
                <div style={{width:"85%",margin:"auto"}}>
                <PhotoCameraBackIcon  sx={{ color:"LightSeaGreen",fontSize: 60 }} />
                <h1 style={{fontWeight:"bold",fontSize:"2em",textTransform:"capitalize"}} >camera</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore laboriosam dignissimos minus saepe aliquam 
                    voluptates voluptatem, illum dicta atque temporibus, id necessitatibus modi distinctio, dolore a eligendi laudantium
                     expedita quam cumque sit? Exercitationem numquam adipisci totam recusandae eos! Culpa, perferendis?</p>
                </div>
                </Item>
            </Grid>
            </Grid>
            <Grid style={{paddingTop:"4em"}} container spacing={1}>
            <Grid xs={12} sm={8} md={5}>
                <Item  >
                   <div style={{width:"80%",margin:"auto"}}>
                   <img style={{width:"100%"}} src="/img/list4.jpg" alt="" />
                   </div>
                </Item>
            </Grid>
            
            <Grid xs={12} sm={12} md={7}>
                <Item  >
                <div style={{width:"85%",margin:"auto"}}>
                <LightModeIcon  sx={{ color:"orange",fontSize: 60 }} />
                <h1 style={{fontWeight:"bold",fontSize:"2em",textTransform:"capitalize"}} >desert</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore laboriosam dignissimos minus saepe aliquam 
                    voluptates voluptatem, illum dicta atque temporibus, id necessitatibus modi distinctio, dolore a eligendi laudantium
                     expedita quam cumque sit? Exercitationem numquam adipisci totam recusandae eos! Culpa, perferendis?</p>
                </div>
                </Item>
            </Grid>
            </Grid>
            </div>
            </section>
        <Footer/>
        </>
    )
}