import DrawerAppBar from "@/content/principal/components/navbar";
import Footer from "@/content/principal/components/Footer"
import { Grid } from "@mui/material"
import Item from "@/content/principal/components/ItemG"
import Carusel from "@/content/principal/components/Carusel";
/* import TourIcon from '@mui/icons-material/Tour'; */
import Mycomponentstwo from "@/content/principal/components/Mycomponentstwo";
import TitlebarImageList from "@/content/principal/components/BarListimg";
import { Tour } from "@mui/icons-material";
export default function AboutRouter(){
    return(
        <>
        <div className="">
        <div>
        <DrawerAppBar/>
        </div>
        <section style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} className="about-one" >
            <div >
            <h2 style={{textAlign:"center",color:"white"}}>OUR SERVICES</h2>
            <h1 style={{textAlign:"center",color:"white"}} className="about-one-section">ABOUT US</h1>
            </div>
            <svg className="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#fff" fillOpacity="1" d="M0,128L80,144C160,160,320,192,480,218.7C640,245,800,267,960,229.3C1120,192,1280,96,1360,48L1440,0L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"></path>
            </svg>
            
        </section>
        
       
        <Grid style={{backgroundColor:"white"}} container spacing={1}>
            <Grid xs={12} sm={12} md={6}>
                <Item >
                    <img style={{width:"100%",padding:"5em"}} src="/img/ss.webp" alt="" />
                </Item>
            </Grid>
            
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} xs={12} sm={12} md={6}>
                <Item style={{paddingTop:"1em"}}>
                <Mycomponentstwo title="Most innovative creative team" paragraph="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet earum incidunt obcaecati assumenda magnam eum, corporis est sed praesentium aut" button="Get Started" />
                </Item>
            </Grid>
            </Grid>
            <div style={{backgroundColor:"white"}}><Carusel/></div>
            <section className="about-two">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#fff" fillOpacity="1" d="M0,224L40,224C80,224,160,224,240,234.7C320,245,400,267,480,282.7C560,299,640,309,720,288C800,267,880,213,960,165.3C1040,117,1120,75,1200,96C1280,117,1360,203,1400,245.3L1440,288L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path>
            </svg>
            <div className="container " style={{textAlign:"center",display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}}>
            <h1 style={{color:"white",fontSize:"3em"}}>SATISFIED CUSTOMERS</h1>
            <Tour  sx={{ color:"white",fontSize: 40 }} />
            <p style={{width:"70%",margin:"auto", color:"white",fontSize:"2.5em"}}>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam recusandae obcaecati excepturi repellendus vitae pariatur."</p>
           
            <span style={{color:"white",fontSize:"1.5em"}}>Walter</span>
            <span style={{color:"DarkOrange",fontSize:"1.5em"}}>operation manager</span>
            </div>
            <svg style={{marginBottom:"-1em"}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#fff" fillOpacity="1" d="M0,64L48,74.7C96,85,192,107,288,144C384,181,480,235,576,218.7C672,203,768,117,864,117.3C960,117,1056,203,1152,224C1248,245,1344,203,1392,181.3L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
            </svg>
            </section>
            {/* <div style={{paddingTop:"3em",paddingBottom:"3em"}}><TitlebarImageList/></div> */}
            <Grid style={{backgroundColor:"white"}} container spacing={1}>
            <Grid xs={12} sm={6} md={4}>
                <Item style={{margin:"4em"}} >
                    <img style={{width:"100%"}} src="/img/list4.jpg" alt="" />
                    <div style={{paddingLeft:"2em"}}>
                    <span style={{color:"DarkOrange",fontSize:"1.5em"}}>operation manager</span>
                    <h1 style={{width:"80%"}}>Crazy the aventure come and visit</h1>
                    </div>
                </Item>
            </Grid>
            <Grid xs={12} sm={6} md={4}>
            <Item style={{margin:"4em"}} >
                    <img style={{width:"100%"}} src="/img/list3.jpg" alt="" />
                    <div style={{paddingLeft:"2em"}}>
                    <span style={{color:"DarkOrange",fontSize:"1.5em"}}>operation manager</span>
                    <h1 style={{width:"80%"}}>Crazy the aventure come and visit</h1>
                    </div>
                </Item>
            </Grid>
            <Grid xs={12} sm={6} md={4}>
            <Item style={{margin:"4em"}} >
                    <img style={{width:"100%"}} src="/img/list2.jpg" alt="" />
                    <div style={{paddingLeft:"2em"}}>
                    <span style={{color:"DarkOrange",fontSize:"1.5em"}}>operation manager</span>
                    <h1 style={{width:"80%"}}>Crazy the aventure come and visit</h1>
                    </div>
                </Item>
            </Grid>
            </Grid>
        <Footer/>
        </div>
        </>
    )
}