import DrawerAppBar from "@/content/principal/components/navbar"
import Footer from "@/content/principal/components/Footer"
import Item from "@/content/principal/components/ItemG";
import { Grid } from "@mui/material";
import DirectionsCarFilledIcon from '@mui/icons-material/DirectionsCarFilled';
import AirlineSeatReclineNormalIcon from '@mui/icons-material/AirlineSeatReclineNormal';
import LocalDiningIcon from '@mui/icons-material/LocalDining';
import HotelIcon from '@mui/icons-material/Hotel';
import Mycomponentstwo from "@/content/principal/components/Mycomponentstwo";
export default function ContactRouter(){
    return(
        <>
        
        <DrawerAppBar/>
        <section style={{display:"flex",justifyContent:"center",alignItems:"center"}} className="Contact-header">
        <h1 className="explorer-h" style={{fontSize:"5em" }}>Contact section</h1>
        <svg className="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#fff" fillOpacity="1" d="M0,288L1440,224L1440,320L0,320Z"></path>
        </svg>
        </section>
        <section style={{backgroundColor:"white"}}>
        <h1 className="explorer-h" style={{fontSize:"4.5em",textTransform:"uppercase", textAlign:"center"}}>offers and prices</h1>
        <p style={{textAlign:"center",fontSize:"1.2em"}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur
         corrupti ducimus quo.</p>
         <Grid container spacing={4}>
            <Grid  xs={12} sm={12} md={12}>
                <Item  >
                <div style={{width:"70%",margin:"auto",paddingTop:"2em",paddingBottom:"2em"}}>
            <div style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
            <DirectionsCarFilledIcon  sx={{ color:"orange",fontSize: 60 }} />
            <div style={{paddingLeft:"1em"}}>
                <h1 className="explorer-h">MOBILITY..................................60$</h1>
                <p className="explorer-h" style={{fontSize:"1.2em"}}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas eaque animi sed saepe?</p>
            </div>
            </div>
         </div>
        
         
         
         
                </Item>
            </Grid>
            
            <Grid  xs={12} sm={12} md={12}>
                <Item >
                <div style={{width:"70%",margin:"auto",paddingTop:"2em",paddingBottom:"2em"}}>
            <div style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
            <AirlineSeatReclineNormalIcon  sx={{ color:"orange",fontSize: 60 }} />
            <div style={{paddingLeft:"1em"}}>
                <h1 className="explorer-h">SITTING......................................60$</h1>
                <p className="explorer-h" style={{fontSize:"1.2em"}}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas eaque animi sed saepe?</p>
            </div>
            </div>
         </div>
                </Item>
            </Grid>
            <Grid  xs={12} sm={12} md={12}>
                <Item >
                <div style={{width:"70%",margin:"auto",paddingTop:"2em",paddingBottom:"2em"}}>
            <div style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
            <LocalDiningIcon  sx={{ color:"orange",fontSize: 60 }} />
            <div style={{paddingLeft:"1em"}}>
                <h1 className="explorer-h">EAT...............................................60$</h1>
                <p className="explorer-h" style={{fontSize:"1.2em"}}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas eaque animi sed saepe?</p>
            </div>
            </div>
         </div>
                </Item>
            </Grid>
            <Grid  xs={12} sm={12} md={12}>
                <Item >
                <div style={{width:"70%",margin:"auto",paddingTop:"2em",paddingBottom:"2em"}}>
            <div style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
            <HotelIcon  sx={{ color:"orange",fontSize: 60 }} />
            <div style={{paddingLeft:"1em"}}>
                <h1 className="explorer-h">HOTEL.........................................60$</h1>
                <p className="explorer-h" style={{fontSize:"1.2em"}}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas eaque animi sed saepe?</p>
            </div>
            </div>
         </div>
                </Item>
            </Grid>
            </Grid>
        </section>
        <section  className="contact">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#fff" fillOpacity="1" d="M0,288L1440,224L1440,0L0,0Z"></path>
        </svg>
            <h1 style={{color:"white",textAlign:"center",fontSize:"3.5em"}}>OPENING HOURS</h1>
            <p style={{color:"white",textAlign:"center",fontSize:"1.2em",paddingBottom:"2em"}}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati tempora </p>
            <Grid style={{display:"flex",alignItems:"center",justifyContent:"center"}} container spacing={1}>
            <Grid xs={12} sm={7} md={6}>
           <div className="contact-end">
           <Item style={{display:"flex",alignItems:"center",justifyContent:"center"}} >
                    <div className="">
                    <div style={{display:"flex",paddingTop:"2em"}}>
                    <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>MONDAY................</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>2PM - 6PM</h1>
                    </div>
                    <div style={{display:"flex",paddingTop:"2em"}}>
                    <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>TUESDAY...............</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>10AM - 6PM</h1>
                    </div>
                    <div style={{display:"flex",paddingTop:"2em"}}>
                    <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>WEDNESDAY........</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>10AM - 6PM</h1>
                    </div>
                    <div style={{display:"flex",paddingTop:"2em"}}>
                    <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>THURSDAY............</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>10AM - 6PM</h1>
                    </div>
                    <div style={{display:"flex",paddingTop:"2em"}}>
                    <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>FRIDAY...................</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>8AM - 8PM</h1>
                    </div>
                    <div style={{display:"flex",paddingTop:"2em"}}>
                    <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>SATURDAY.............</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>8AM - 6PM</h1>
                    </div>
                    <div style={{display:"flex",paddingTop:"2em",paddingBottom:"2em"}}>
                     <h1 style={{color:"white",textAlign:"center",fontSize:"1.5em"}}>SUNDAY..................</h1>
                    <h1 style={{color:"OrangeRed",textAlign:"center",fontSize:"1.5em"}}>8AM - 6PM</h1>
                    </div>
                    </div>
                </Item>
           </div>
            </Grid>
            </Grid>
        <svg style={{marginBottom:"-1em"}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#fff" fillOpacity="1" d="M0,288L1440,224L1440,320L0,320Z"></path>
        </svg>
        </section>
        <section>
        <Grid style={{backgroundColor:"white"}} container spacing={1}>
            <Grid xs={12} sm={12} md={6}>
                <Item >
                    <img style={{width:"100%",padding:"5em"}} src="/img/ss.webp" alt="" />
                </Item>
            </Grid>
            
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} xs={12} sm={12} md={6}>
                <Item style={{paddingTop:"1em"}}>
                <Mycomponentstwo title="Most innovative creative team" paragraph="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet earum incidunt obcaecati assumenda magnam eum, corporis est sed praesentium aut" button="Get Started" />
                </Item>
            </Grid>
            </Grid>
        </section>
        <Footer/>
        </>
    )
}