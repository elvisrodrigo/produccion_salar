import { Grid ,Button } from "@mui/material"
import Mycomponentstwo from "@/content/principal/components/Mycomponentstwo"
import Item from "@/content/principal/components/ItemG"
import Footer from "@/content/principal/components/Footer"
import DrawerAppBar from "@/content/principal/components/navbar"
import PersonIcon from '@mui/icons-material/Person';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import FolderOpenIcon from '@mui/icons-material/FolderOpen';
export default function Gallery(){
    return (
        <>
        
        <DrawerAppBar/>
        
        <section style={{display:"flex",justifyContent:"center",alignItems:"center"}} className="Gallery-header">
        
        <svg className="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#fff" fillOpacity="1" d="M0,192L40,186.7C80,181,160,171,240,186.7C320,203,400,245,480,
        234.7C560,224,640,160,720,160C800,160,880,224,960,240C1040,256,1120,224,1200,192C1280,160,1360,128,1400,
        112L1440,96L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,
        320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
        </svg>
        <h1 className="explorer-h" style={{fontSize:"5em" }}>Gallery section</h1>
        </section>
        <section>
        <Grid style={{backgroundColor:"white"}} container spacing={1}>
            <Grid xs={12} sm={12} md={6}>
                <Item >
                    <img style={{width:"100%",padding:"5em"}} src="/img/ss.webp" alt="" />
                </Item>
            </Grid>
            
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} xs={12} sm={12} md={6}>
                <Item style={{paddingTop:"1em"}}>
                <Mycomponentstwo title="Most innovative creative team" paragraph="Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                Amet earum incidunt obcaecati assumenda magnam eum, corporis est sed praesentium aut" button="Get Started" />
                </Item>
            </Grid>
            </Grid>
        </section>
        <section>
            <div className="container">
            <div className=" container">
            <Grid container spacing={1}>
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}}  xs={12} sm={12} md={6}>
                <Item style={{}} >
                    <div style={{margin:"3em"}}>
                    <img style={{width:"100%"}} src="/img/list2.jpg" alt="" />
                    <div style={{width:"85%",margin:"auto"}}>
                    <h1>HOW TO BUILD A CLIENTELE LOREM IPSUM DOLOR</h1>
                    <div style={{display:"flex",paddingBottom:"2em"}} >
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><PersonIcon  sx={{ color:"black",fontSize: 25 }} />johntanedo</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><AccessTimeIcon  sx={{ color:"black",fontSize: 25 }} />Feb 8 2021</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><FolderOpenIcon  sx={{ color:"black",fontSize: 25 }} />Uncategorized</span>
                    </div>
                    <p>How to build a clientele Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum leo nec 
                        eros egestas, non sagittis urna vehicula. Nunc mauris nibh, porta ut nulla eu, imperdiet interdum urna. Nunc vel 
                        felis lectus. Maecenas euismod a tortor...</p>
                        <div style={{float:"right",marginBottom:"2em"}}><Button className="button-five" style={{marginTop:"1em"}} variant="contained">
                              <span>VIEW FULL POST</span>
                            </Button></div>
                    </div>
                    </div>
                </Item>
            </Grid>
            
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} xs={12} sm={12} md={6}>
                <Item >
                <div style={{margin:"3em"}}>
                    <img style={{width:"100%"}} src="/img/list3.jpg" alt="" />
                    <div style={{width:"85%",margin:"auto"}}>
                    <h1>HOW TO BUILD A CLIENTELE LOREM IPSUM DOLOR</h1>
                    <div style={{display:"flex",paddingBottom:"2em"}} >
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><PersonIcon  sx={{ color:"black",fontSize: 25 }} />johntanedo</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><AccessTimeIcon  sx={{ color:"black",fontSize: 25 }} />Feb 8 2021</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><FolderOpenIcon  sx={{ color:"black",fontSize: 25 }} />Uncategorized</span>
                    </div>
                    <p>How to build a clientele Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum leo nec 
                        eros egestas, non sagittis urna vehicula. Nunc mauris nibh, porta ut nulla eu, imperdiet interdum urna. Nunc vel 
                        felis lectus. Maecenas euismod a tortor...</p>
                        <div style={{float:"right",marginBottom:"2em"}}><Button className="button-five" style={{marginTop:"1em"}} variant="contained">
                              <span>VIEW FULL POST</span>
                            </Button></div>
                    </div>
                    </div>
                </Item>
            </Grid>
            </Grid>
            <Grid container spacing={1}>
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}}  xs={12} sm={12} md={6}>
                <Item style={{}} >
                    <div style={{margin:"3em"}}>
                    <img style={{width:"100%"}} src="/img/list5.jpg" alt="" />
                    <div style={{width:"85%",margin:"auto"}}>
                    <h1>HOW TO BUILD A CLIENTELE LOREM IPSUM DOLOR</h1>
                    <div style={{display:"flex",paddingBottom:"2em"}} >
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><PersonIcon  sx={{ color:"black",fontSize: 25 }} />johntanedo</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><AccessTimeIcon  sx={{ color:"black",fontSize: 25 }} />Feb 8 2021</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><FolderOpenIcon  sx={{ color:"black",fontSize: 25 }} />Uncategorized</span>
                    </div>
                    <p>How to build a clientele Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum leo nec 
                        eros egestas, non sagittis urna vehicula. Nunc mauris nibh, porta ut nulla eu, imperdiet interdum urna. Nunc vel 
                        felis lectus. Maecenas euismod a tortor...</p>
                        <div style={{float:"right",marginBottom:"2em"}}><Button className="button-five" style={{marginTop:"1em"}} variant="contained">
                              <span>VIEW FULL POST</span>
                            </Button></div>
                    </div>
                    </div>
                </Item>
            </Grid>
            
            <Grid style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center"}} xs={12} sm={12} md={6}>
                <Item >
                <div style={{margin:"3em"}}>
                    <img style={{width:"100%"}} src="/img/list6.jpg" alt="" />
                    <div style={{width:"85%",margin:"auto"}}>
                    <h1>HOW TO BUILD A CLIENTELE LOREM IPSUM DOLOR</h1>
                    <div style={{display:"flex",paddingBottom:"2em"}} >
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><PersonIcon  sx={{ color:"black",fontSize: 25 }} />johntanedo</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><AccessTimeIcon  sx={{ color:"black",fontSize: 25 }} />Feb 8 2021</span>
                    <span style={{display:"flex",justifyContent:"center",alignItems:"center",textTransform:"uppercase",paddingRight:".7em"}}><FolderOpenIcon  sx={{ color:"black",fontSize: 25 }} />Uncategorized</span>
                    </div>
                    <p>How to build a clientele Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum leo nec 
                        eros egestas, non sagittis urna vehicula. Nunc mauris nibh, porta ut nulla eu, imperdiet interdum urna. Nunc vel 
                        felis lectus. Maecenas euismod a tortor...</p>
                        <div style={{float:"right",marginBottom:"2em"}}><Button className="button-five" style={{marginTop:"1em"}} variant="contained">
                              <span>VIEW FULL POST</span>
                            </Button></div>
                    </div>
                    </div>
                </Item>
            </Grid>
            </Grid>
            </div>
            </div>
        </section>
        <Footer/>
        </>
    )
}