import { useState, useEffect } from "react";
import { Box,Button } from "@mui/material";
import Grid from "@mui/material/Grid";
import Item from '@/content/principal/components/ItemG';
import Footer from '@/content/principal/components/Footer';
import Gallery from '@/content/principal/components/Gallery';
import Mycomponentsfive from '@/content/principal/components/Mycomponentsthree';
import Mycomponentsone from '@/content/principal/components/Mycomponentsone';
import Mycomponentstwo from '@/content/principal/components/Mycomponentstwo';
import DrawerAppBar from '@/content/principal/components/navbar';
import Carusel from '@/content/principal/components/Carusel';
import AOS from 'aos';
import 'aos/dist/aos.css';

function Hero() {
  const [showComponet, setShowComponet] = useState(false);
  //variable que va a guardar lo que es el estado de el componente 
  //permitir cambiar el estado de ese componente
  // use effect hook permite sincronizar un componente con us sistema externo
  useEffect(() => {
    AOS.init();
    //
    const timig = window.performance.timing;
    /*  */
    const duration = timig.loadEventEnd - timig.navigationStart;
    //loadEventEnd momento en el que finalizo la carga de un recurso 
    //navigationstart el tiempo que en el que se inicio la navegacion en una pagina web
    window.setTimeout(() => {
      //establecer el tiempo de espera
      setShowComponet(true);
    }, duration);
  });
     
  return (
    <>
    {!showComponet ? (
      <Load />
    ) : (
   <div>
    <section className="section-one">
      <video className="video" width="100%" autoPlay muted loop>
        <source src="/video/web-cam.mp4" />
      </video>
      {/* <HeaderG  /> */}
      <DrawerAppBar/>
      <div className="section-one-content">
        <div className="container">
          <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={12} md={5}>
                <Item>
                  
                  <div data-aos="fade-up" className="container">
                    <h1 style={{fontSize:"6em",color:"white"}}>Salar de Uyuni</h1>
                    <p className="dance-p" style={{ width: "70%" }}>
                      Lorem ipsum dolor sit amet consectetur adipisicing
                      elit. Est, ab. Lorem ipsum dolor sit amet
                      consectetur, adipisicing elit.
                    </p>
                    <Button className="button-header" variant="contained">
                      <span>Get Started</span>
                    </Button>
                  </div>
                </Item>
              </Grid>

              
             {/*  <Grid item xs={7}>
              <Box  className="dance second" sx={{ flexGrow: 1 }}>
                  <div data-aos="flip-left"data-aos-easing="ease-out-cubic" data-aos-duration="2000" className='dance-second'>
                                            <Grid container spacing={1}>
                                              <Grid item xs={4}>
                                                <Item>
                                                  <div className='card-1'>
                                                  <h1 >01</h1>
                                                  <h2>Concert</h2>
                                                  <p>Lorem, ipsum dolor.</p>
                                                  </div>
                                                  </Item>
                                              </Grid>
                                              <Grid item xs={4}>
                                                <Item>
                                                <div className='card-1'>
                                                  <h1 >25</h1>
                                                  <h2>Concert</h2>
                                                  <p>Lorem, ipsum dolor.</p>
                                                  </div>
                                                  </Item>
                                              </Grid>
                                              <Grid item xs={4}>
                                                <Item>
                                                <div className='card-1'>
                                                  <h1 >30</h1>
                                                  <h2>Concert</h2>
                                                  <p>Lorem, ipsum dolor.</p>
                                                  </div>
                                                  </Item>
                                              </Grid>
                                            </Grid>
                                            </div>
                </Box>
              </Grid> */}
              
            </Grid>
          </Box>
        </div>
      </div>
    </section>
    <section className="section-two" style={{ paddingBottom: "4em" }}>
      <div className="container" >
        <div data-aos="flip-left"data-aos-easing="ease-out-cubic" data-aos-duration="2000">
          <h6
            className="four"
            style={{ textAlign: "center", paddingTop: "3em" }}
          >
            BUILD TRUST FIRST
          </h6>
        </div>
        <h1 data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000" className="section-two-h">World concerts</h1>
        <Grid container spacing={1}>
              <Grid item xs={12} sm={6} md={3} >
                <Item>
                <Mycomponentsone data-aos="flip-right" data-aos-duration="2000" imageUrl="/img/sal-6.jpg" icon="21st may 2020 by admin" colorText="HotPink" title="Ready To Be" paragraph="Lorem ipsum dolor sit amet Rerum autem aspernatur sit"
                  button="Get Started" />
                </Item>
              </Grid>
              <Grid item xs={12} sm={6} md={3} >
                <Item>
                <Mycomponentsone data-aos="flip-right" data-aos-duration="2000" imageUrl="/img/sal-7.jpg" icon="24st april 2020 by admin" colorText="MediumAquamarine" title="Celebrate" paragraph="Lorem ipsum dolor sit amet Rerum autem aspernatur sit"
                  button="Get Started" />
                </Item>
              </Grid>    
              <Grid item xs={12} sm={6} md={3} >
                <Item>
                <Mycomponentsone data-aos="flip-right" data-aos-duration="2000" imageUrl="/img/sal-8.jpg" icon="04st sept 2020 by admin" colorText="LightSalmon" title="Between" paragraph="Lorem ipsum dolor sit amet Rerum autem aspernatur sit"
                  button="Get Started" />
                </Item>
              </Grid> 
              <Grid item xs={12} sm={6} md={3} >
                <Item>
                <Mycomponentsone data-aos="flip-right" data-aos-duration="2000" imageUrl="/img/sal-9.jpg" icon="09st dic 2020 by admin" colorText="SkyBlue" title="Moonlight Surise" paragraph="Lorem ipsum dolor sit amet Rerum autem aspernatur sit"
                  button="Get Started" />
                </Item>
              </Grid> 
            </Grid>
<div style={{paddingTop:"5em"}}>

        <Grid container spacing={1}>
      <Grid item xs={12} sm={12} md={6}>
        <Item>
        <Mycomponentstwo title="Most innovative creative team" paragraph="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet earum incidunt obcaecati assumenda magnam eum, corporis est sed praesentium aut" button="Get Started" />
        </Item>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <Item>  
           <div style={{display:"flex",justifyContent:"center",alignItems:"center",flexDirection:"column",margin:"auto"}}>
           <div data-aos="fade-left" data-aos-duration="2000" className="img-cont">
           <div className="img1"></div>
           <div className="img2"></div>
           </div>
           </div>
        </Item>
      </Grid>
    </Grid>
</div>
      </div>
    </section>
    <section className="section-three">
      <div className="container">
        <div
          style={{
            width: "40%",
            float: "right",
            textAlign: "right",
            marginRight: "15em",
            marginBottom: "5em",
          }}
        >
          <h6 data-aos="flip-left"  data-aos-easing="ease-out-cubic" data-aos-duration="2000" className="four" style={{ paddingBottom: "1em" }}>
            WHAT WE DO
          </h6>
          <h1 data-aos="flip-left"  data-aos-easing="ease-out-cubic" data-aos-duration="2000" className="three-sect">
            Service to solve all kinds of business problem
          </h1>
        </div>
      </div>
      <div className="container">
        <div className="gallery" id="gallery">
          <div className="box-container">
            <Gallery imageUrl="/img/lunch.jpg" name="Lunch" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. At, ipsam?" buttonValue="Read More " />
            <Gallery imageUrl="/img/breakfast.jpg" name="Freakfast" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. At, ipsam?" buttonValue="Read More " />
            <Gallery imageUrl="/img/dinner.jpg" name="Dinner" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. At, ipsam?" buttonValue="Read More " />
            <Gallery imageUrl="/img/hotel.jpg" name="Hotel" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. At, ipsam?" buttonValue="Read More " />
            <Gallery imageUrl="/img/movil.jpg" name="Mobility" description="Lorem ipsum dolor sit amet consectetur adipisicing elit. At, ipsam?" buttonValue="Read More " />

          </div>
        </div>
      </div>
    </section>
    
    <section className="section-four"></section>
    <section className="section-five container">

   <div style={{marginBottom:"4em"}}> <Mycomponentsfive direction={true} imageUrl="/img/sal-3.avif" colorText="HotPink" titles="Development" title="Jodi kokhono vul hoye jay tumi oporadh nio na"
              paragraph="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sequi optio quis assumenda. ipsum dolor sit amet consectetur adipisicing elit. Sequi optio quis assumenda." button="View Project" /></div>
    <div style={{marginBottom:"4em"}}><Mycomponentsfive direction={false} colorText="MediumAquamarine" titles="Research & Analytics" title="Ei biristi veja rate tuminei bole somoy amar katena"
              paragraph="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sequi optio quis assumenda. ipsum dolor sit amet consectetur adipisicing elit. Sequi optio quis assumenda." button="View Project"  imageUrl="/img/sal-4.jpg"/></div>
      <Mycomponentsfive direction={true} imageUrl="/img/sal-2.jpg" colorText="LightSalmon" titles="UI/UX Engineering" title="Ami jare chaire se thake mori ontore"
              paragraph="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sequi optio quis assumenda. ipsum dolor sit amet consectetur adipisicing elit. Sequi optio quis assumenda." button="View Project" />
      
    </section>
    <section>
            <Carusel/>
     {/*  <div data-aos="fade-up">
        <div className="slider">
          <ul>
            <li>
              <img
                className="carusel-img" src="/img/sal-1.jpg" alt="" />
            </li>
            <li>
              <img className="carusel-img" src="/img/sal-2.jpg" alt="" />
            </li>
            <li>
              <img className="carusel-img" src="/img/sal-3.avif" alt="" />
            </li>
            <li>
              <img className="carusel-img" src="/img/sal-4.jpg" alt="" />
            </li>
            <li>
              <img className="carusel-img" src="/img/sal-5.avif" alt="" />
            </li>
            <li>
              <img
                className="carusel-img" src="/img/sal-6.jpg" alt=""
              />
            </li>
          </ul>
        </div>
      </div> */}
    </section>
    
    <Footer />
          
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
  
   </div>
    )}
    </>
  );
}
function Load() {
  return (
    <div className="LOADER">
      <div className="LOADERA">
        <figure className="loader">
          <div className="dot white"></div>
          <div className="dot"></div>
          <div className="dot"></div>
          <div className="dot"></div>
          <div className="dot"></div>
        </figure>
      </div>
    </div>
  );
}
export default Hero;
