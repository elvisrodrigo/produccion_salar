
import {Link, Button } from "@mui/material";
import LibraryMusicIcon from "@mui/icons-material/LibraryMusic";
import MenuIcon from '@mui/icons-material/Menu';
const tour = {
  color: "#fff !important",
  fontweight: "bold",
  fontsize: "1.5rem !important",
  texttransform: "capitalize",
}

export default function HeaderG(props: any) {
  return (
    <>
       <header data-aos="fade-down-left" data-aos-delay="200">
              <span>
                <Link className="tour" href="#" underline="hover">
                  Tour
                </Link>
              </span>
              <div className="navegation">
                <div className="navegation-items">
                  <Link className="" href="/" underline="hover">
                    Home
                  </Link>
                  <Link className="" href="/tour/About" underline="hover">
                    About
                  </Link>
                  <Link className="" href="/tour/Explorer" underline="hover">
                    Explorer
                  </Link>
                  <Link className="" href="/tour/Gallery" underline="hover">
                    Gallery
                  </Link>
                  <Link className="" href="/tour/Contact" underline="hover">
                    Contact
                  </Link>
                </div>
              </div>
              <span className="iconn-header1">
                         <div className="navegation1">
                <div className="navegation-items1">
                  <Link className="sing" href="/auth" underline="hover">
                    Sing in
                  </Link>
                </div>
              </div>
              </span>
            </header>
    </>
  )
}

{/* <header data-aos="fade-down-left" data-aos-delay="200">
              <span>
                <Link className="tour" href="#" underline="hover">
                  Tour
                </Link>
              </span>
              <div className="navegation">
                <div className="navegation-items">
                  <Link className="" href="/" underline="hover">
                    Home
                  </Link>
                  <Link className="" href="/tour/About" underline="hover">
                    About
                  </Link>
                  <Link className="" href="/tour/Explorer" underline="hover">
                    Explorer
                  </Link>
                  <Link className="" href="/tour/Gallery" underline="hover">
                    Gallery
                  </Link>
                  <Link className="" href="/tour/Contact" underline="hover">
                    Contact
                  </Link>
                </div>
              </div>
              <span className="iconn-header1">
                         <div className="navegation1">
                <div className="navegation-items1">
                  <Link className="sing" href="/auth" underline="hover">
                    Sing in
                  </Link>
                </div>
              </div>
              </span>
            </header> */}