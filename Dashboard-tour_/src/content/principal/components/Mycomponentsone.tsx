import { Button } from "@mui/material";
export default function Mycomponentsone(props: any) {
  return (

    <>
      <div data-aos="fade-up"
     data-aos-easing="ease-out-cubic"
     data-aos-duration="2000" className="blogs-second">
        <img 
          className="img-second"
          style={{
            width: "80%",
            marginTop: "20px",
            marginLeft: "20px",
            marginRight: "20px",
          }}
          src={props.imageUrl}
          alt=""
        />
        <div className="icons">
          <a href="#">{props.icon}</a>

        </div>
        <h1 style={{ color: props.colorText }} >{props.title}</h1>
        <p
          style={{
            width: "80%",
            paddingTop: "10px",
            paddingBottom: "10px",
            margin: "auto",
          }}
        >
          {props.paragraph}
        </p>
        <Button className="button-five" variant="contained">
          <span>{props.button}</span>
        </Button>
      </div>
    </>
  )
}


const HotPink = "HotPink"/*rosado*/
const MediumAquamarine = "MediumAquamarine"/*verde*/
const LightSalmon = "LightSalmon"/*naranja*/
const SkyBlue = "SkyBlue"
