import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

/* function createNavElements(name: string, ref: string) {
  return { name, ref }
}

const navItems = [
  createNavElements('Home', '/'),
  createNavElements('About', '/tour/About'),
  createNavElements('Explorer', 'tour/Explorer'),
  createNavElements('Gallery', '/tour/Gallery'),
  createNavElements('Contact', '/tour/Contact'),
] */
const drawerWidth = 240;
/* const navItems = ['Home', 'About', 'Explorer','Gallery','Contact']; */

export default function DrawerAppBar(props: Props) {
  const { window } = props;
  /* const [click2, setClick2] = React.useState(); */
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };


  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 3 }}>
        <div className="tour1">
          TOUR
        </div>
      </Typography>
      <Divider />
      <List>
        {/* {navItems.map((item) => (
          <ListItem key={item} disablePadding> */}
            <ListItemButton sx={{ textAlign: 'center' }}>
              {/* <ListItemText primary={item} /> */}
              <div className='navbarresponsive' style={{display:"flex",flexDirection:"column"}}>
              <a href='/'>
                    Home
                    
                  </a>
                
              
                  <a href='/tour/Explorer'>
                    Explorer
                  </a>
                  <a href='/tour/Gallery'>
                    Gallery
                  </a>
                  <a href='/tour/Contact' >
                    Contact
                  </a>
                  <a href='/tour/About'>
                    About
                  </a>
                  <a className="sing" href="/auth" >
                    Sing in
                  </a></div>
            </ListItemButton>
         {/*  </ListItem>
        ))} */}
      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <>
    <header>
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar style={{backgroundColor:"black",paddingLeft:"3em",paddingRight:"3em"}}  component="nav">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
          >
            <div className='tour'>
              tour
            </div>
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {/* {navItems.map((item) => (
              <Button key={item} sx={{ color: '#fff' }}>
                <div className="navegation">
                <div className="navegation-items">
                {item}
                </div>
              </div>
              
              </Button>
            ))} */}
             <div style={{display:"flex",justifyContent:"space-between"}}>
             <div className="navegation">
                <div className="navegation-items">
                  <a href='/'>
                    Home
                  </a>
                 
                  <a href='/tour/Explorer'>
                    Explorer
                  </a>
                  <a href='/tour/Gallery'>
                    Gallery
                  </a>
                  <a href='/tour/Contact' >
                    Contact
                  </a>
                  <a href='/tour/About' >
                    About
                  </a>
                </div>
              </div>
              <div className="navegation1">
                <div className="navegation-items1">
                  <a className="sing" href="/auth" >
                    Sing in
                  </a>
                </div>
              </div>
             </div>
              
          </Box>
        </Toolbar>
       
      </AppBar>
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
      <Box component="main" sx={{ p: 3 }}>
        <Toolbar />
       
      </Box>
    </Box>
    </header>
    </>
  );
}
