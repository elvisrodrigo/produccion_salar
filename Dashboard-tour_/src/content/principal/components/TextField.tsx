import { Box,TextField, TextFieldProps} from '@mui/material'

export function Item(props: TextFieldProps) {
    const { sx, ...other } = props;
    return (
        <Box
        component="form"
        sx={{
          
          '& > :not(style)': { m: 1, width: '25ch' ,color:"red"},
        }}
        noValidate
        autoComplete="off"
      >
        
      </Box>
  
    );
} 