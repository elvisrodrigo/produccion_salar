import {Box,  Button} from "@mui/material";
import Grid from "@mui/material/Grid";
import Item from "./ItemG";
export default function Mycomponentsfive(props:any) {
    return (
        
        <> 
            {props.direction ?(
              <Grid container spacing={1}>
              <Grid item xs={12} sm={12} md={6}>
                <Item>
                <div data-aos="fade-right" data-aos-duration="2000" id="five" style={{display:"block", margin:"auto"}}>
                <img className="img-five" src={props.imageUrl} alt="" />
                </div>
                </Item>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Item>
                <div data-aos="flip-left"
                             data-aos-easing="ease-out-cubic"
                            data-aos-duration="2000" style={{ width: "60%", margin: "auto" }}>
                            <p style={{color:props.colorText}}>{props.titles}</p>
                            <h1 style={{fontSize:"2.5em",paddingTop:".5em",paddingBottom:".5em"}}>{props.title}</h1>
                            <p>{props.paragraph}</p>
                            <Button className="button-five" style={{marginTop:"1em"}} variant="contained">
                              <span>{props.button}</span>
                            </Button>
                          </div>
                </Item>
              </Grid>
            </Grid>
            
            ) :
            (
              <Grid container spacing={1}>
              <Grid item xs={12} sm={12} md={6}>
                <Item>
                <div data-aos="flip-left"
                             data-aos-easing="ease-out-cubic"
                            data-aos-duration="2000" style={{ width: "60%", margin: "auto" }}>
                            <p style={{color:props.colorText}}>{props.titles}</p>
                            <h1 style={{fontSize:"2.5em",paddingTop:".5em",paddingBottom:".5em"}}>{props.title}</h1>
                            <p>{props.paragraph}</p>
                            <Button className="button-five" style={{marginTop:"1em"}} variant="contained">
                              <span>{props.button}</span>
                            </Button>
                          </div>
                </Item>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Item>
                <div data-aos="fade-right" data-aos-duration="2000" id="five" style={{display:"block", margin:"auto"}}>
                               <img className="img-five" src={props.imageUrl} alt="" />
                              </div>
                </Item>
              </Grid>
            </Grid>
            )}

        </>
    )
}


const HotPink="HotPink"/*rosado*/
const MediumAquamarine="MediumAquamarine"/*verde*/ 
const LightSalmon="LightSalmon"/*naranja*/ 

Mycomponentsfive.defaultProps = {
  direction:true
}