
import { Document } from 'mongoose';

export interface Travel extends Document {
    readonly title: string;
    readonly description: string;
    readonly body: string;
    readonly author: string;
    
}