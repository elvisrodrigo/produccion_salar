import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Travel } from '../interfaces/travel.interface';
import { TravelDto } from '../dto/travel.dto';


@Injectable()
export class TravelService {
    constructor(@InjectModel('Travel') private readonly travelModel: Model<Travel>) { }
    async createTravel(TravelDto:TravelDto){
        const newPost =await new this.travelModel(TravelDto);
        return newPost.save();
    }
}
