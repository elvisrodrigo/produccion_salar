import { ApiProperty } from "@nestjs/swagger";

export class TravelDto {
    @ApiProperty()
    readonly title: string;
    @ApiProperty()
    readonly description: string;
    @ApiProperty()
    readonly body: string;
    @ApiProperty()
    readonly author: string;
}