
import * as mongoose from 'mongoose';

export const TravelSchema = new mongoose.Schema({
    title: String,
    description: String,
    body: String,
    author: String,
    slug: String
},
{
    timestamps: {
      createdAt: 'created_at', // Use `created_at` to store the created date
      updatedAt: 'updated_at' // and `updated_at` to store the last updated date
    }
  }
)