import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder , SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
  .setTitle('DocumentBuilder')
  .setDescription('Document')
  .setVersion('1.0')
  //.addTag('product')
  .build();
  const document = SwaggerModule.createDocument(app, options );
  SwaggerModule.setup('api',app,document);
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
 