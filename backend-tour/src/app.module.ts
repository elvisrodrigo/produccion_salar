import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TravelesModule } from './blog/modules/traveles.module';
@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://public:NApFgAc5XwkbaLaR@cluster0.e1zztdg.mongodb.net/?retryWrites=true&w=majority'),
    TravelesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
